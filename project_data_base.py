from os import system, name         # این کتاب خانه برای  عملیات پاک کردن
                                   # صفحه خروجی ترمینال به برنامه اضافه شده 

green = '\033[1;32;40m'
red = '\033[31m'
endc = '\033[0m'
orange = "\033[33m"

def clear():                        # تابع پاک کننده صفحه ترمینال 
  
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 

# یک نمونه از کلاس لینک لیست 
class Node : 

    def __init__(self , name ) : 
        self.name = name 
        self.next = None 
        self.point = 0


class Link_list() : 

    def __init__(self ) :
        self.head = None
    
    # این متد برای لینک لیست  گره تازه 
    # ایجاد میسازد منظور از گره تاز همان 
    # ایجاد یک گره میباشد 
    # ارگومان دریافتی این متد فقط نام متد میباشد 
    def create_node_to_link_list(self , tim_name) :
        created_node = Node(tim_name)
        return created_node

    def sort_link_list(self , temp) :

        previous = self.head 
        current = self.head.next 

        if temp.point>= self.head.point :
            temp.next = self.head
            self.head = temp 
            return
        # ادد کردن در جای مناسب 
        while(current is not None ) :
            if temp.point >= current.point : 
                previous.next = temp
                temp.next = current
                return
            else :
                current = current.next 
                previous = previous.next
        # اگر امتیاز از همه امتیاز ها کمتر باشد
        if current is None : 
            previous.next = temp 
            return

    def Add_new_tim_to_link_list(self,new_node):
        

        if (self.head is None )  : 
            self.head = new_node

        elif self.find_specific_tim(added_tim_name) :
            print('\ntim with this name already exist in table')

        
        else :
            self.sort_link_list(new_node)


    
    def display_table(self) : 
        temp = self.head
        print('Name\t\t\t\t\tPoint')
        if temp is None :
            print('\nthere is no tim in the table ')
        while(temp is not None) : 
            print('{}\t\t\t\t\t{}'.format(temp.name,temp.point))
            temp = temp.next

    def find_specific_tim(self , tim_name) : 
        temp = self.head 
        
        while(temp is not None) : 
            if temp.name == tim_name :
                return temp
            temp = temp.next
        return False
    
    def update_tim(self) :
        tim_name = input("Enter tim name that you want to update : ")
        result = self.delete_specific_tim(tim_name)

        if result :
            print('current --> tim name:{} \t\t tim points:{}'.format(result.name , result.point))
            try :
                new_name = input('Enter the new name of the tim : ')
                if new_name == '' :
                    f
                new_point = int(input('Enter new points of the tim : '))
                result.name = new_name
                result.point = new_point
            except :
                print ('the point or name must have value or characters ')
                print('update is canceled')

            if self.head is None :
                self.Add_new_tim_to_link_list(result)
            else :
                self.sort_link_list(result)
        else:
            print('\ntim with this name doesnt Exist in the table ')


    def delete_specific_tim(self,tim_name) :
        
        if self.head is None :
            return False 

        elif self.head.name == tim_name :
            current = self.head 
            self.head = self.head.next
            current.next = None
            return current

        else:
            previous = self.head
            current = self.head.next
            while(current is not None ) :
                if current.name == tim_name :
                    previous.next = current.next
                    current.next = None 
                    return current
                else:
                    current = current.next
                    previous = previous.next
            return False

    def seatch_teams(self,tim_name) :
        temp = self.head 
        results = []
        
        while(temp is not None) : 
            if temp.name.__contains__(tim_name):
                results.append(temp)
            temp = temp.next
            if(temp == None):
                return results
            

table = Link_list()



while(True) :
    clear()
    print('1.display table')
    print('2.add new tim to table')
    print('3.finde specific tim in the table')
    print('4.Update tim name and point')
    print('5.delete one specific tim ')
    print('6.Searech Teams')
    choose = int(input('Select one of above options : '))
    print("\n\n")
    if choose == 1 : 
        table.display_table()
    elif choose == 2 : 
        added_tim_name = input("Enter new team name that you want to add the link list : ")
        new_node = table.create_node_to_link_list(added_tim_name)
        table.Add_new_tim_to_link_list(new_node)

    elif choose == 3 : 
        tim_name = input('Enter team name to search: ')
        tim = table.find_specific_tim(tim_name)
        if tim :
            print(green + "\nteam exist in tabel " + endc)
            print('\nname:{}\t\tpoints:{}'.format(tim.name , tim.point))
        else:
            print(red + '\nteam doesnt exist in table' + endc)
    elif choose == 4 : 
        table.update_tim()
    elif choose == 5 : 
        tim_name = input("Enter the team name that you want to delete : ")
        result = table.delete_specific_tim(tim_name)
        if result :
            print(green + 'delete is done!' + endc) 
        else:
            print(red + 'we dont find team with this name!' + endc)
    elif choose == 6 : 
        tim_name = input("Enter the team name that you want to delete : ")
        result = table.seatch_teams(tim_name)
        if result:
            print("---------------------------------")
            print(green + "found(" + str(len(result)) + ") result!" + endc)
            for team in result:
                print('\nname:{}\t\tpoints:{}'.format(team.name , team.point))

    input(orange + '\npress any button to continue' + endc)
    clear()
